Add the contents of the add_to_inlist.txt file to the controls section of your inlist file and add replace the run_star_extras.f file in your src folder with the one provided.
Recompile your project directory by executing an './mk' command so the changes to the run_star_extras.f file take effect.
Follow the instructions provided in the add_to_inlist.txt file for turning the ejection yield recorders on and off.

The burst number is now also recorded in the history.data file as an extra column, and indicates the current burst that the model is on. 

NOTE: When your using 'x_logical_ctrl(2) = .true', there is a little noise in the last few values of the mean ejected yeilds of the species at the end of each burst. Therefore, to get a the accurate mean values, don't use the values which have a few zeroes inbetween at the end of the burst. 
